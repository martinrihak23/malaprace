# Statické stránky generované z Markdownu s CI/CD

Tento projekt demonstruje vytvoření a nasazení statických webových stránek pomocí generátoru statických stránek a kontinuální integrace (CI) na GitLabu. Projekt zahrnuje generování HTML stránek z Markdown souborů a testování kvality Markdownu jako součást CI pipeline. 

## Obsah

1. [Úvod](#úvod)
2. [Použití](#použití)
3. [Instalace](#instalace)
4. [Prerekvizity](#prerekvizity)
5. [CI/CD Pipeline](#cicd-pipeline)
6. [Deployment](#deployment)
7. [Doporučení a zdroje](#doporučení-a-zdroje)

## Úvod

Tento projekt slouží jako příklad pro vytvoření statických webových stránek z Markdown souborů. Používá generátor statických stránek MKDocs a kontinuální integraci na GitLabu k automatizaci procesu generování HTML a testování kvality Markdownu.

GitLab CI/CD pipeline provádí následující kroky:
- Test kvality Markdown souborů.
- Generování HTML stránek z Markdown zdrojů.
- Automatické nasazení (deploy) webových stránek.

## Použití

1. **Úprava Markdown souborů**: Upravit Markdown soubory v adresáři `docs/`. Každý změněný soubor bude automaticky testován na kvalitu a po úspěšném testování bude z něj vygenerována HTML stránka.
  
2. **CI/CD Pipeline**: Po každém commitu do repozitáře se automaticky spustí CI/CD pipeline, která provede všechny definované kroky a nasadí aktualizované stránky na veřejně dostupnou URL.

## Instalace

1. **Klonování repozitáře**:
    ```bash
    git clone https://gitlab.com/uzivatel/jmeno_repozitare.git
    cd jmeno_repozitare
    ```

2. **Instalace závislostí**:
    - Ujistěte se, že máte nainstalován Python (verze 3.7 nebo novější).
    - Nainstalujte potřebné balíčky:
    ```bash
    pip install -r requirements.txt
    ```

3. **Generování stránek lokálně**:
    ```bash
    mkdocs build
    ```

4. **Lokální spuštění stránek**:
    ```bash
    mkdocs serve
    ```
    Stránky budou dostupné na `http://127.0.0.1:8000`.

## Prerekvizity

- **Python**: Verze 3.7 nebo novější.
- **MkDocs**: Nástroj pro generování statických stránek z Markdown souborů.
- **Git**: Správa verzí.
- **GitLab**: Pro hostování kódu a CI/CD pipeline.
